import org.junit.jupiter.api.BeforeEach;

@ExtendWith({TextReportExtension.class})
public class forTest {

    @BeforeEach
    void 1234567() {
        System.setProperty("chromeoptions.mobileEmulation", "deviceName=Nexus 5");
        Configuration.baseUrl = "https://master.autospot.ru";
        //цуаукцамцукамц
        WebDriverRunner.getWebDriver().manage().deleteCookieNamed("exp-16096");
        WebDriverRunner.getWebDriver().manage().addCookie(new Cookie("exp-16096", "b"));
    }

    @RepeatedTest(44)
    @DisplayName("Отправка заявки Володьку")
    void sendRequestWithPageVolodka() {
        open("/brands/audi/a8/sedan/price/");
        Selenide.executeJavaScript("vueApp.openVolodkaPopup()");
        $(by("data-element-marker-id", "popup-volodka_button_params")).click();
        $$(".button--small").get(0).click();
        $$(".button--small").get(2).click();
        $(byText("К оформлению заявки")).click();
    }

    @Test
    @DisplayName("Отправка заявки через Володьку")
    void sendRequestWithPageVolodka() {
        open("/brands/audi/a8/sedan/price/");
        Selenide.executeJavaScript("vueApp.openVolodkaPopup()");
        $(by("data-element-marker-id", "popup-volodka_button_params")).click();
        $$(".button--small").get(0).click();
        $(byText("К оформлению заявки")).click();
    }
}
